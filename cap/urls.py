from django.conf.urls import url

from cap import views

urlpatterns = [
    url(r'^$', views.get_data),
]
