from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.
import json
import random

def business_analyze(request):
    '''
    /chart_capital?
    t=[type int]&
    c=[lat float],[lng float]&
    r=[radius(m) int]&
    range=[timestamp(x1000) long],[timestamp(x1000) long]

    request:
        /chart_capital?t=1&c=24.123456,121.123456&r=5000&range=1401580800000,1406851200000

    response:
        [
            {
            "timestamp": 1401580800000,
            "value": 50000
            },
            {
            "timestamp": 1404172800000,
            "value": 60000
            },
            {
            "timestamp": 1406851200000,
            "value": 40000
            }
        ]
    '''

    can_set = [[0.64, 0.35, 0.26, 0.17, 0.09], [0.7, 0.6, 0.4, 0.3, 0.2], [0.75, 0.54, 0.32, 0.24, 0.07], [0.82, 0.67, 0.55, 0.44, 0.32]]
    time_set = [1405555200,1437091200,1468713600,1500249600,1531785600]
    
    docs = []
    can_ = can_set[random.randint(0,3)]
    print can_
    for idx, time_can in enumerate(time_set):
        docs.append({"timestamp":time_can, "value":can_[idx]})
    return HttpResponse(json.dumps(docs, indent = 4, ensure_ascii=False).encode("utf-8"))

