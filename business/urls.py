from django.conf.urls import url

from business import views

urlpatterns = [
    url(r'^_analyze', views.business_analyze),
    #url(r'^_interval$', views.map_interval),
    #url(r'^_num$', views.chart_num),
]
