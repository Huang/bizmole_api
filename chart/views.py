# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponse
import pysolr
# Create your views here.
import json
import pprint
import datetime
from time import gmtime, strftime



solr = pysolr.Solr('http://solr:8983/solr/biz/', timeout=10)




def search_solr(q, params):
    rows = 100
    remaining_count = 1
    first_run_flag = True
    docs = []
    start = 0
    params["start"] = 0

    while(remaining_count > 0):
        # start:[ * TO  1213891200] AND end:[ 1213891200 TO *] AND class:1
        params["start"] += rows
        result = solr.search(q, **params)
        for doc in result:
            docs.extend([doc])
        if first_run_flag:
            first_run_flag = False
            remaining_count = result.hits
        remaining_count -=rows
        start += rows
    return docs

def map_interval(request):

    r_class = request.GET["t"]
    r_center = request.GET["c"]
    r_radius = request.GET["r"]
    r_timestamp = request.GET["at"]

    q = "start:[* TO %s] AND end:[%s TO *] AND class:%s " % (r_timestamp, r_timestamp, r_class)
    params = {"fq": "{!bbox}", "pt": r_center, "sfield": "latlng", "d": str(float(r_radius) / 1000)}
    docs = search_solr(q, params)

    return HttpResponse(json.dumps(docs, indent = 4, ensure_ascii=False).encode("utf-8"))


def args_parser(request, params_list):

    result_dict = {}
    params_list = ["range", "t", "lat", "lng"]
    for k in params_list:
        if k not in request.GET:
            print k + "not in request.get"

    result_dict["start"], result_dict["end"]  = request.GET["range"].split(",")
    result_dict["class"] = request.GET["t"]
    result_dict["center"] = 0


'''
def generate_season_grid(start, end, 30):
    # -*- coding: utf-8 -*-
    interval = 60*60*24*30*1000
    (start - end) / interval
    pass

'''

def chart_num(request):
    # 24.969481,121.192516
    # latlng
    # d = 2
    '''
    /chart_num?
    t=[type int]&
    c=[lat float],[lng float]&
    r=[radius(m) int]&
    range=[timestamp(x1000) long],[timestamp(x1000) long]

    request:
        /chart_capital?t=1&c=24.123456,121.123456&r=5000&range=1401580800000,1406851200000

     [
         {
         "timestamp": 1401580800000,
         "value": 50
         },
         {
         "timestamp": 1404172800000,
         "value": 60
         },
         {
         "timestamp": 1406851200000,
         "value": 40
         }
     ]
    '''


    r_class = request.GET["t"]
    r_center = request.GET["c"]
    r_radius = request.GET["r"]
    s_timestamp, e_timestamp = request.GET["range"].split(",")
    chart_interval = 2592000 # 60*60*24*30
    duration_list = range( int(s_timestamp), int(e_timestamp), int(chart_interval))
    #print duration_list

    docs = []
    response = []
    print strftime("%Y-%m-%d %H:%M:%S", gmtime())
    for t in duration_list:
        q = "start:[* TO %s] AND end:[%s TO *] AND class:%s" % (t, t, r_class)
        params = {"fq": "{!bbox}", "pt": r_center, "sfield": "latlng", "d": str(float(r_radius) / 1000), "rows":1 , "fl":"sn"}
        result = solr.search(q, **params)
        response.append({"timestamp":t, "value":result.hits})
    print strftime("%Y-%m-%d %H:%M:%S", gmtime())
    return HttpResponse(json.dumps(response, indent = 4, ensure_ascii=False).encode("utf-8"))
    #return HttpResponse(test)


def get_search_docs(params):
    docs = []
    params = {"fq": "{!geofilt}", "pt": "24.969481,121.192516", "sfield": "latlng", "d": 1, "rows":200}
    #docs.extend([i for i in solr.search("*", **params)])
    result = solr.search(q, **params)
    docs.extend([i for i in result])
    print result.hits


def chart_capital(request):
    '''
    /chart_capital?
    t=[type int]&
    c=[lat float],[lng float]&
    r=[radius(m) int]&
    range=[timestamp(x1000) long],[timestamp(x1000) long]

    request:
        /chart_capital?t=1&c=24.123456,121.123456&r=5000&range=1401580800000,1406851200000

    response:
        [
            {
            "timestamp": 1401580800000,
            "value": 50000
            },
            {
            "timestamp": 1404172800000,
            "value": 60000
            },
            {
            "timestamp": 1406851200000,
            "value": 40000
            }
        ]
    '''

    r_class = request.GET["t"]
    r_center = request.GET["c"]
    r_radius = request.GET["r"]
    s_timestamp, e_timestamp = request.GET["range"].split(",")

    chart_interval = 2592000 # 60*60*24*30
    duration_list = range( int(s_timestamp), int(e_timestamp), int(chart_interval))

    docs = []
    response = []
    print "S:" + strftime("%Y-%m-%d %H:%M:%S", gmtime())
    for t in duration_list:
        summation = 0
        q = "start:[* TO %s] AND end:[%s TO *] AND class:%s" % (t, t, r_class)
        params = {"fq": "{!bbox}", "pt": r_center, "sfield": "latlng", "d": str(float(r_radius) / 1000), "fl":"capital", "stats": "true", "stats.field": "capital", "rows":0}
        result = solr.search(q, **params)
        #for doc in result:
        #summation += doc["capital"]
        #print result.stats['stats_fields']['capital']['sum']
        summation = result.stats['stats_fields']['capital']['sum']
        hits = result.hits
        response.append({"timestamp":t, "value":summation/hits})
    print "E:" + strftime("%Y-%m-%d %H:%M:%S", gmtime())
    return HttpResponse(json.dumps(response, indent = 4, ensure_ascii=False).encode("utf-8"))





'''
import datetime
print(
datetime.datetime.fromtimestamp(
int("1284101485")
).strftime('%Y-%m-%d %H:%M:%S')
)
'''
