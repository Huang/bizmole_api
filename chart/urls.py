from django.conf.urls import url

from chart import views

urlpatterns = [
    url(r'^_capital$', views.chart_capital),
    url(r'^_interval$', views.map_interval),
    url(r'^_num$', views.chart_num),
]
