# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponse
import pysolr
# Create your views here.
import json
import pprint
import datetime
import io
from time import gmtime, strftime, time

from scipy.spatial.distance import pdist, squareform
from scipy.cluster.hierarchy import linkage
from sklearn.feature_extraction.text import CountVectorizer
# Create your views here.

import jieba
import jieba.analyse
from scipy.cluster.hierarchy import fcluster,fclusterdata


#jieba.set_dictionary('/tmp/dict.txt.big')


def create_stopword():
    stopword_dict, stopword_list = {}, []
    stopword_list.extend(["XD", "XDD", "XDDD", "XDDDD"])
    stopword_list.extend(["..", "...", "....", "....."])
    stopword_list.extend(["ptt", "cc", "http", "telnet", "Re", "www",  "Fw"])
    stopword_list.extend([u"轉錄", u"電視", u"爆卦", u"新聞", u"媒體"])
    stopword_list.extend([u"引述", u"八卦", u"之銘言", u"有沒有", u"問卦"])
    for stopword in stopword_list:
        stopword_dict[stopword] = True
    return stopword_dict


def load_model():
    ptt_docs = get_ptt_docs(start=0)
    data_dict, data_id_list, feature_list = get_feature_list(ptt_docs)
    data_array = get_feature_vector(feature_list)
    cluster_result = get_clusters(data_dict, data_id_list, data_array)
    return data_dict, cluster_result


def load_news_model():
    news_docs = get_solr_docs(start=0)
    data_dict, data_id_list, feature_list = get_feature_list(news_docs)
    data_array = get_feature_vector(feature_list)
    cluster_result = get_clusters(data_dict, data_id_list, data_array)
    return data_dict, cluster_result


def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


def get_ptt_docs(start=0):
    result_set = []
    rows = 500
    solr = pysolr.Solr('http://solr.recast.ncu.edu.tw/solr/ptt_core/', timeout=15)
    q = "*:*"
    docs = []
    timestamp = "[" + str(int(time()) - 60*60*12) + " TO *]"
    fq = ["{!frange l=3}sum(num_likes, num_dislikes, num_comments)",  "timestamp:"+timestamp ]
    params = {"fq": fq, "start":start, "rows":rows, "sort":"sum(num_likes, num_dislikes, num_comments) desc"}
    # "pt": "24.969481,121.192516", "sfield": "latlng", "d": 1, 
    #docs.extend([i for i in solr.search("*", **params)])                                                                                                    
    result = solr.search(q, **params)
    result_set.extend([i for i in result])
    print result.hits
    #if  result.hits  > start:
    #    result_set.extend(get_ptt_docs(start=(start + rows)))
    return result_set



def get_solr_docs(start=0):
    result_set = []
    rows = 500
    solr = pysolr.Solr('http://solr.recast.ncu.edu.tw/solr/tw_news/', timeout=15)
    q = "*:*" 
    docs = []
    fq = "timestamp:[1408924800 TO *]"
    params = {"fq": fq, "start":start, "rows":500, "sort": " timestamp desc"}
    # "pt": "24.969481,121.192516", "sfield": "latlng", "d": 1, 
    #docs.extend([i for i in solr.search("*", **params)])                                                                                                    
    result = solr.search(q, **params)
    result_set.extend([i for i in result])
    print result.hits
    if  result.hits  > start:
        result_set.extend(get_solr_docs( start=(start + rows)))
    return result_set



def get_feature_list(docs):
    feature_list = []
    data_labels = []
    stopword_dict = create_stopword()
    data_id_list = []
    data_likes_dict = {}
    data_comments_dict = {}
    data_content_dict = {}
    data_title_list = []
    data_title_dict = {}
    data_videos_dict = {}
    data_images_dict = {}
    data_media_dict = {}
    i  = 0
    for doc in docs:
        try:
            if 'title' not in doc:
                continue
            feature = ""
            feature = doc['title']
            i += 1
            #tmp = get_summary(content, topS=2)
            #tags = jieba.analyse.extract_tags(tmp, topK=3)
            tags = jieba.analyse.extract_tags(feature, topK=8)
            tags_result = []

            for tag in tags:
                if tag in stopword_dict or is_number(tag) :
                    continue
                tags_result.append(tag)

            feature_list.append(" ".join(tags_result))
            data_id_list.append(doc['id'])

            likes = 0
            if 'likes' in doc:
                likes = doc['likes']
            data_likes_dict[doc['id']] = likes

            comments = 0
            if 'comments' in doc:
                comments = doc['comments']
            data_comments_dict[doc['id']] = comments

            data_title_list.append(doc['title'])
            data_title_dict[doc['id']] = doc['title']

            content = ""
            if 'content' in doc:
                content = doc['content']
            data_content_dict[doc['id']] = content
            
            media = ""
            if 'media' in doc:
                media = doc['media']
            data_media_dict[doc['id']] = media

            videos = []
            if 'videos' in doc:
                videos = doc['videos']
            data_videos_dict[doc['id']] = videos

            images = []
            if 'images' in doc:
                images = doc['images']
            data_images_dict[doc['id']] = images

        except ValueError as e:
            print  e.message
            print ">", i, feature ,"<"
            continue

    data_dict = {'likes':data_likes_dict, 'comments':data_comments_dict, 'content': data_content_dict, 'title':data_title_dict, 'videos':data_videos_dict, 'images':data_images_dict, 'media':data_media_dict}
    return data_dict, data_id_list, feature_list


def get_feature_vector(feature_list):
    c = CountVectorizer()
    bow_array = c.fit_transform(feature_list)

    data_array = []
    for row in bow_array.toarray():
        data_array.append(row.tolist())
    #print len(data_array), len(c.get_feature_names())
    return data_array


def get_clusters(data_dict, data_id_list, data_array):
    data_dist = pdist(data_array) # computing the distance
    data_link = linkage(data_dist, method="complete") # computing the linkage

    clusting_result = fcluster(data_link,t=2.5 ,criterion='distance',depth=1000,R=None,monocrit=None)#这个需要先计算linkage，再出结果

    id_doc_set = {}
    for object_id, cluster_id  in zip(data_id_list, clusting_result):
        if cluster_id not in id_doc_set:
            id_doc_set[cluster_id] = []
        id_doc_set[cluster_id].append(object_id)

    cluster_result = {}
    i = 0
    for k in sorted(id_doc_set, key=lambda k: len(id_doc_set[k]), reverse=True):
        #print k, "\n".join(id_doc_set[k])
        #print "\n"
        cluster_result[i] = id_doc_set[k]
        i += 1
    return cluster_result


timeout_value = 500
last_update_timestamp = time()
data_dict, model = load_news_model()

#model = load_model()
def save_model(model):
    #with io.open('model.json', 'w', encoding='utf8') as json_file:
    #    json_string = json.dumps(model,ensure_ascii=False).encode("utf-8")
    #    json_file.
    with open('model.json', 'w') as json_file:
       json.dump(model, json_file, sort_keys = True, indent = 4)


def save_news_model(data_dict, model):
    #with io.open('model.json', 'w', encoding='utf8') as json_file:
    #    json_string = json.dumps(model,ensure_ascii=False).encode("utf-8")
    #    json_file.
    with open('model.json', 'w') as json_file:
       json.dump(model, json_file, sort_keys = True, indent = 4)
    with open('data_dict.json', 'w') as json_file2:
       json.dump(data_dict, json_file2, sort_keys = True, indent = 4)


def get_topics123(request):
    global last_update_timestamp
    #if time() - last_update_timestamp > timeout_value:
    if time() - last_update_timestamp > timeout_value:
        model = load_model()
        save_model(model)
        last_update_timestamp = time()
    else:
        json_data=open('model.json')
        model = json.load(json_data)
    model['time'] = time()
    return HttpResponse(json.dumps(model, indent = 4, ensure_ascii=False, sort_keys = True).encode("utf-8"))


def get_cluster_thumb(data_dict, cluster):
    thumb_hotest_score = 0
    hotest_image = ""
    for object_id in cluster:
        if len(data_dict['images'][object_id]) == 0:
            continue
        if thumb_hotest_score < data_dict['likes'][object_id]:
            hotest_image = data_dict['images'][object_id][0]
    return hotest_image


def get_cluster_report_counts(cluster):
    return len(cluster)


def get_cluster_media_counts(data_dict, cluster):
    media_list = []
    for object_id in cluster:
        media_list.append(data_dict['media'][object_id])
    return len(set(media_list))


def get_cluster_threat_level(data_dict, cluster):
    return 0


def get_cluster_title(data_dict, cluster):
    hotest_title = ""
    hotest_count = 0
    for object_id in cluster:
        if data_dict['likes'][object_id] >= hotest_count:
            hotest_title = data_dict['title'][object_id]
            hotest_count = data_dict['likes'][object_id]
    return hotest_title


def get_cluster_likes(data_dict, cluster):
    like_counts = 0
    for object_id in cluster:
        like_counts += data_dict['likes'][object_id]
    return like_counts


def get_cluster_comments(data_dict, cluster):
    comment_counts = 0
    for object_id in cluster:
        comment_counts += data_dict['comments'][object_id]
    return comment_counts


def get_cluster_snippet(data_dict, cluster):
    snippet = " " * 10000
    for object_id in cluster:
        content = data_dict['content'][object_id]
        if len(content) > 1 and len(content) < snippet:
            snippet = content
    return snippet


def get_topics(request):
    """
    http://140.115.157.87:18080/api_get_topics
    INPUT:
    {
            'c': INT <topic counts>,
    }
    OUTPUT:
    [
        {
            'topic_id': INT,
            'media_counts': INT <the number of medias reports this topic> ,
            'reports_counts': INT <the number of reports has been reported>,
            'comments_counts': INT <the number of comments related to this topic>,
            'likes': INT <the number of likes related to this topic>,
            'dislikes': INT <the number of dislikes related to this topic>,
            'threat_level': INT <the thread level of this topic>,
            'category': STRING <the category of this topic>,
            'title': STRING <the title of this topic>,
            'snippet':  TEXT <the snippet of this topic>,
            'district':  STRING <the district of this topic>,
            'latlng': STRING  <the latlng of this topic>,
            'thumb': STRINF <the link of thumb file>,
            's_timestamp': INT <the timestamp of this topic>,
            'e_timestamp': INT <the timestamp of this topic>
        }, ...
    ]
    """


    global last_update_timestamp
    global data_dict, model
    #if time() - last_update_timestamp > timeout_value:
    if (time() - last_update_timestamp )> timeout_value:
        data_dict, model = load_news_model()
        save_news_model(data_dict, model)
        last_update_timestamp = time()
    """
    else:
        json_data=open('model.json')
        model = json.load(json_data)
        json_data=open('data_dict.json')
        data_dict = json.load(json_data)
    """
    '''
    print "!!!"
    data_dict, model = load_news_model()
    '''

    print type(model)
    group = 0
    docs = []
    for k in model:
        print len(model[k])
        group += 1
        if group == 1:
            continue
        if group == 6:
            break


        doc = {}
        doc['topic_id'] = (group - 1)
        doc['media_counts'] = get_cluster_media_counts(data_dict, model[k])
        doc['reports_counts'] = get_cluster_report_counts(model[k])
        doc['comments_counts'] = get_cluster_comments(data_dict, model[k])
        doc['likes'] =  get_cluster_likes(data_dict, model[k])
        doc['dislikes'] = 0
        doc['threat_level'] = get_cluster_threat_level(data_dict, model[k])
        doc['category'] = ""
        doc['title'] = get_cluster_title(data_dict, model[k])
        doc['snippet'] = get_cluster_snippet(data_dict, model[k])
        doc['district'] = ""
        doc['latlng'] = ""
        doc['thumb'] = get_cluster_thumb(data_dict, model[k])
        doc['s_timestamp'] = 0
        doc['e_timestamp'] = 0
        docs.append(doc)

    '''
    doc = {
        'topic_id': 5,
        'media_counts': 3,
        'reports_counts': 2,
        'comments_counts': 12,
        'likes': 23,
        'dislikes': 11,
        'threat_level': 1,
        'category': 'politics',
        'title': 'Taiwan cross-strait nigo problem',
        'snippet': 'Taiwan are about to start a cross-strait nigotiation on military trusting',
        'district':  'TAIWAN',
        'latlng': '24,123, 123.212',
        'thumb': '',
        's_timestamp': 1405987200,
        'e_timestamp': 0
    }
    docs.append(doc)
    '''

    return HttpResponse(json.dumps(docs, indent = 4, ensure_ascii=False).encode("utf-8"))


def get_topic(request):
    """
    INPUT:
    {
        'topic_id': INT <TOPIC ID>,
    }
    OUTPUT:
    {
        'topic_id': INT,
        'media_counts': INT <the number of medias reports this topic> ,
        'reports_counts': INT <the number of reports has been reported>,
        'comments_counts': INT <the number of comments related to this topic>,
        'likes': INT <the number of likes related to this topic>,
        'dislikes': INT <the number of dislikes related to this topic>,
        'threat_level': INT <the thread level of this topic>,
        'category': STRING <the category of this topic>,
        'title': STRING <the title of this topic>,
        'snippet':  TEXT <the snippet of this topic>,
        'district':  STRING <the district of this topic>,   
        'latlng': STRING  <the latlng of this topic>,
        'thumb': STRINF <the link of thumb file>,
        's_timestamp': INT <the timestamp of this topic>,
        'e_timestamp': INT <the timestamp of this topic>,

        'hashtags': [ STRING, STRING],

        'news_list':[{INT<object_id>:STRING<link>}, ...
                     {INT<object_id>:STRING<link>}],
        'social_links':[{INT<object_id>:STRING<link>}, ...
                        {INT<object_id>:STRING<link>}],
        'youtube_links':[{INT<object_id>:STRING<link>}, ...
                         {INT<object_id>:STRING<link>}],
        'pic_list':[{INT<object_id>:STRING<link>}, ...
                    {INT<object_id>:STRING<link>}],

        'trend_discuss': {INT<timestamp>:INT<value>,  ....},
        'trend_likes':{INT<timestamp>:INT<value>,  ....},
        'trend_dislikes':{INT<timestamp>:INT<value>,  ....},
        'trend_spread': {INT<timestamp>:STRING<object id>, ... }

    }
    """


    doc = {
        'topic_id': 5,
        'media_counts': 3,
        'reports_counts': 2,
        'comments_counts': 12,
        'likes': 23,
        'dislikes': 11,
        'threat_level': 1,
        'category': 'politics',
        'title': 'Taiwan cross-strait nigo problem',
        'snippet': 'Taiwan are about to start a cross-strait nigotiation on military trusting',
        'district':  'TAIWAN',
        'latlng': '24,123, 123.212',
        'thumb': '',
        's_timestamp': 1405987200,
        'e_timestamp': 0,

        'hashtags': ['politics', 'china', 'taiwan'],

        'news_list': {1:'http://www.yahoo.com.tw/news/123.htm',
                      2:'http://www.yahoo.com.tw/news/345.htm'},
        'social_links': {1:'http://www.facebook.com/1234123231231/',
                         2:'http://www.facebook.com/1231423124142/'},
        'youtube_links': {1:'http://www.youtube.com/p12341241',
                          2: 'http://www.youtube.com/12312314'},
        'pic_list': {1:'http://www.img.url/123.jpg',
                     2: 'http://www.img.url/3221.jpg'},

        'trend_discuss': { 1400716800: 1245, 1403395200:5323, 1405987200:12},
        'trend_likes': { 1400716800: 245, 1403395200:2323, 1405987200:142},
        'trend_dislikes': { 1400716800: 2245, 1403395200:323, 1405987200:12},
        'trend_spread': { 1400716800: 2415, 1403395200:223, 1405987200:12},
    }

    return HttpResponse(json.dumps(doc, indent = 4, ensure_ascii=False).encode("utf-8"))


def get_news_by_id(request):
    """
    INPUT:
    {
            'c': INT <topic counts>,
    }
    OUTPUT:
    [
        {
            'topic_id': INT,
            'media_counts': INT <the number of medias reports this topic> ,
            'reports_counts': INT <the number of reports has been reported>,
            'comments_counts': INT <the number of comments related to this topic>,
            'likes': INT <the number of likes related to this topic>,
            'dislikes': INT <the number of dislikes related to this topic>,
            'threat_level': INT <the thread level of this topic>,
            'category': STRING <the category of this topic>,
            'title': STRING <the title of this topic>,
            'snippet':  TEXT <the snippet of this topic>,
            'district':  STRING <the district of this topic>,   
            'latlng': STRING  <the latlng of this topic>,
            'thumb': STRINF <the link of thumb file>,
            's_timestamp': INT <the timestamp of this topic>,
            'e_timestamp': INT <the timestamp of this topic>
        }, ...
    ]
    """

    docs = []
    doc = {
        'topic_id': 5,
        'media_counts': 3,
        'reports_counts': 2,
        'comments_counts': 12,
        'likes': 23,
        'dislikes': 11,
        'threat_level': 1,
        'category': 'politics',
        'title': 'Taiwan cross-strait nigo problem',
        'snippet': 'Taiwan are about to start a cross-strait nigotiation on military trusting',
        'district':  'TAIWAN',
        'latlng': '24,123, 123.212',
        'thumb': '',
        's_timestamp': 1405987200,
        'e_timestamp': 0
    }
    docs.append(doc)

    return HttpResponse(json.dumps(docs, indent = 4, ensure_ascii=False).encode("utf-8"))

def get_notice(request):
    """
    INPUT:
    {
        'center': STRING <latlng>,
        'radius': STRING <Radius>
    }
    OUTPUT:
    [
        {'object_id': STRING, 'latlng': STRING, 'title':STRING, rank: INT , thread_level:INT ,snippet:STRING},
        {'object_id': STRING, 'latlng': STRING, 'title':STRING, rank: INT , thread_level:INT ,snippet:STRING},
        {'object_id': STRING, 'latlng': STRING, 'title':STRING, rank: INT , thread_level:INT ,snippet:STRING},
    ]

    """
    docs = []
    doc = {'object_id': 1, 'latlng': "23.123,123.12", 'title':"This is title", rank: 1 , thread_level:1 ,snippet:"This is snippet"} * 3

    docs.append(doc)

    return HttpResponse(json.dumps(docs, indent = 4, ensure_ascii=False).encode("utf-8"))



def get_notice(request):
    """
    INPUT:
    {
            'c': INT <topic counts>,
    }
    OUTPUT:
    [
        {
            'topic_id': INT,
            'media_counts': INT <the number of medias reports this topic> ,
            'reports_counts': INT <the number of reports has been reported>,
            'comments_counts': INT <the number of comments related to this topic>,
            'likes': INT <the number of likes related to this topic>,
            'dislikes': INT <the number of dislikes related to this topic>,
            'threat_level': INT <the thread level of this topic>,
            'category': STRING <the category of this topic>,
            'title': STRING <the title of this topic>,
            'snippet':  TEXT <the snippet of this topic>,
            'district':  STRING <the district of this topic>,   
            'latlng': STRING  <the latlng of this topic>,
            'thumb': STRINF <the link of thumb file>,
            's_timestamp': INT <the timestamp of this topic>,
            'e_timestamp': INT <the timestamp of this topic>
        }, ...
    ]
    """

    docs = []
    doc = {
        'topic_id': 5,
        'media_counts': 3,
        'reports_counts': 2,
        'comments_counts': 12,
        'likes': 23,
        'dislikes': 11,
        'threat_level': 1,
        'category': 'politics',
        'title': 'Taiwan cross-strait nigo problem',
        'snippet': 'Taiwan are about to start a cross-strait nigotiation on military trusting',
        'district':  'TAIWAN',
        'latlng': '24,123, 123.212',
        'thumb': '',
        's_timestamp': 1405987200,
        'e_timestamp': 0
    }
    docs.append(doc)

    return HttpResponse(json.dumps(docs, indent = 4, ensure_ascii=False).encode("utf-8"))

