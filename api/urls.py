from django.conf.urls import url

from api import views

urlpatterns = [
    url(r'^_get_topics$', views.get_topics),
    url(r'^_get_topic$', views.get_topic),
    url(r'^_get_notice$', views.get_notice),
]

