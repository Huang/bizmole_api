from django.conf.urls import patterns, include, url

from django.contrib import admin
from cap import views
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'rest_api.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^map', include('chart.urls')),
    url(r'^cap/$', include('cap.urls')),
    url(r'^chart', include('chart.urls')),
    url(r'^business', include('business.urls')),
)
